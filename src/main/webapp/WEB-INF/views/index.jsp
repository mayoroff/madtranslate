<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>



<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport"    content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">

    <title>MadTranslator</title>

    <!-- Bootstrap itself -->
    <link href="http://netdna.bootstrapcdn.com/bootstrap/3.0.3/css/bootstrap.min.css" rel="stylesheet" type="text/css">

    <!-- Custom styles -->
    <link rel="stylesheet" href="css/magister.css">

    <!-- Fonts -->
    <link href="http://netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <link href='http://fonts.googleapis.com/css?family=Wire+One' rel='stylesheet' type='text/css'>

</head>

<body class="theme-invert">

<!--<nav class="mainmenu">
    <div class="container">
        <div class="dropdown">
            <button type="button" class="navbar-toggle" data-toggle="dropdown"><span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span> </button>
            <!-- <a data-toggle="dropdown" href="#">Dropdown trigger</a> -->
     <!--       <ul class="dropdown-menu" role="menu" aria-labelledby="dLabel">
                <li><a href="#head" class="active">Hello</a></li>
                <li><a href="#about">About me</a></li>
                <li><a href="#themes">Themes</a></li>
                <li><a href="#contact">Get in touch</a></li>
            </ul>
        </div>
    </div>
</nav> -->

<!-- Main (Home) section -->
<section class="section" id="head">
    <div class="container">

        <div class="row">
            <div class="col-md-10 col-lg-10 col-md-offset-1 col-lg-offset-1 text-center">

                <!-- Site Title, your name, HELLO msg, etc. -->
                <h1 class="title">Mad Translator</h1>
                <h2 class="subtitle">proof-of-concept</h2>

                <h3>Вставьте текст в поле ниже</h3>
                <form id="inputForm" action="madify" method="post">
                    <p><textarea id="textArea" class="form-control btn-default" rows ="15" name="text"></textarea></p>
                    <p><input id="submitButton" type="submit" value="Мэдифицировать" class="btn btn-default btn-lg"> с помощью сервиса <a href="http://translate.yandex.ru">Яндекс.Перевод</a></p>
                    <label id="result"></label>
                </form>

            </div> <!-- /col -->
        </div> <!-- /row -->

    </div>
</section>

<!-- Load js libs only when the page is loaded. -->
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
<script src="http://malsup.github.io/jquery.form.js"></script>
<script src="http://netdna.bootstrapcdn.com/bootstrap/3.0.3/js/bootstrap.min.js"></script>
<script src="js/modernizr.custom.72241.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/spin.js/1.2.7/spin.min.js"></script>
<!-- Custom template scripts -->
<script src="js/magister.js"></script>
<script>
    // wait for the DOM to be loaded
    $(document).ready(function() {

        var options = {
            beforeSubmit:  showRequest,  // pre-submit callback
            success:       showResponse,  // post-submit callback
            dataType:  'json'        // 'xml', 'script', or 'json' (expected server response type)
        };

        $('#inputForm').ajaxForm(options);
    });

    function showRequest(formData, jqForm, options) {
        var target = document.getElementById('#inputForm');
        spinner.spin($('#inputForm')[0]);
        $('#head').find('input, textarea, button, select').attr('disabled','disabled');
        return true;
    }

    function showResponse(responseText, statusText, xhr, $form)  {
        $("#textArea").val(responseText);
        spinner.stop();
        $('#head').find('input, textarea, button, select').removeAttr('disabled');
    }
</script>
<script>
    var opts = {
        lines: 13, // The number of lines to draw
        length: 20, // The length of each line
        width: 10, // The line thickness
        radius: 30, // The radius of the inner circle
        corners: 1, // Corner roundness (0..1)
        rotate: 0, // The rotation offset
        direction: 1, // 1: clockwise, -1: counterclockwise
        color: '#000', // #rgb or #rrggbb or array of colors
        speed: 1, // Rounds per second
        trail: 60, // Afterglow percentage
        shadow: false, // Whether to render a shadow
        hwaccel: false, // Whether to use hardware acceleration
        className: 'spinner', // The CSS class to assign to the spinner
        zIndex: 2e9, // The z-index (defaults to 2000000000)
        top: '50%', // Top position relative to parent
        left: '100%' // Left position relative to parent
    };
    var target = document.getElementById('head');
    var spinner = new Spinner(opts).spin();

</script>
</body>
</html>

