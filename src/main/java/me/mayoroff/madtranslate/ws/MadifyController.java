package me.mayoroff.madtranslate.ws;


import me.mayoroff.madtranslate.core.MadTranslate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.ws.rs.core.MediaType;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;


/**
 * Created by Arseni on 22.06.2014.
 */
@Controller
@RequestMapping("/madify")
@Service
public class MadifyController {

    @Value("${madtranslate.iterations}")
    private String iterations;

    @Autowired
    private MadTranslate madTranslate;

    @RequestMapping(method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON)
    @ResponseBody
    protected List<String> madifyRequest(@RequestParam("text") String text) throws IOException {

        List<String> list = new ArrayList<String>();
        list.add(madTranslate.madify(text, Integer.parseInt(iterations)));

        return list;
    }
}