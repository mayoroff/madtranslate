package me.mayoroff.madtranslate.core;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.ws.rs.core.MultivaluedMap;
import java.util.List;
import java.util.Random;

/**
 * Created by Arseni on 22.06.2014.
 */
@Service
public class MadTranslate {

    @Autowired
    private TranslateAPI translateAPI;
    MultivaluedMap<Language, Language> translations;
    Random rng = new Random();

    public MadTranslate() {
    }

    public String madify(String text, int iterations) {
        translations = translateAPI.getTranslations();

        Language textLanguage = translateAPI.detectLanguage(text);
        Language originLanguage = textLanguage;

        for (int i = 0 ; i < iterations; i++) {
            List<Language> destinationLangs = translations.get(textLanguage);
            if (destinationLangs.size() == 0) {
                break;
            }
            Language destinationLanguage = destinationLangs.get(rng.nextInt(destinationLangs.size()));
            text = translateAPI.translate(text, textLanguage, destinationLanguage);
            textLanguage = destinationLanguage;
        }

        text = translateAPI.translate(text, textLanguage, originLanguage);

        return text;
    }

}
