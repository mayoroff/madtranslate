package me.mayoroff.madtranslate.core;

import javax.ws.rs.core.MultivaluedMap;

/**
 * Created by Arseni on 22.06.2014.
 */
public interface TranslateAPI {

    Language detectLanguage(String text);

    MultivaluedMap<Language, Language> getTranslations();

    String translate(String text, Language from, Language to);
}
