package me.mayoroff.madtranslate.core.yandex;

import me.mayoroff.madtranslate.core.Language;

/**
 * Created by Arseni on 22.06.2014.
 */
public class YandexAPILanguageImpl extends Language {
    private String value;

    public YandexAPILanguageImpl(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        YandexAPILanguageImpl that = (YandexAPILanguageImpl) o;

        return value.equals(that.value);

    }

    @Override
    public int hashCode() {
        return value.hashCode();
    }

    @Override
    public String toString() {
        return value;
    }
}
