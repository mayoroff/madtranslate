package me.mayoroff.madtranslate.core.yandex;

import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.api.client.config.ClientConfig;
import com.sun.jersey.api.client.config.DefaultClientConfig;
import com.sun.jersey.core.util.MultivaluedMapImpl;
import me.mayoroff.madtranslate.core.Language;
import me.mayoroff.madtranslate.core.TranslateAPI;
import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedHashMap;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.UriBuilder;

/**
 * Created by Arseni on 22.06.2014.
 */
@Service
public class YandexTranslateAPI implements TranslateAPI {

    @Value("${yandex.translate.key}")
    private String key;
    @Value("${yandex.translate.translate_url}")
    private String translateUrl;
    @Value("${yandex.translate.getlangs_url}")
    private String getLangsUrl;
    @Value("${yandex.translate.detect_url}")
    private String detectUrl;


    private ClientResponse makeRequest(String url, MultivaluedMap<String, String> formData) {
        ClientConfig config = new DefaultClientConfig();
        Client client = Client.create(config);
        WebResource service = client.resource(UriBuilder.fromUri(url).build());
        return service.type(MediaType.APPLICATION_FORM_URLENCODED).accept(MediaType.APPLICATION_JSON).post(ClientResponse.class, formData);
    }

    @Override
    public Language detectLanguage(String text) {
        MultivaluedMap<String,String> formData = new MultivaluedMapImpl();
        formData.add("key", key);
        formData.add("text", text);
        ClientResponse response = makeRequest(detectUrl, formData);

        String result = "";
        String responseString = response.getEntity(String.class);
        try {
            result = new JSONObject(responseString).getString("lang");
        } catch (JSONException e) {
            e.printStackTrace();
            System.err.println(responseString);
        }

        return new YandexAPILanguageImpl(result);
    }

    @Override
    public MultivaluedMap<Language, Language> getTranslations() {

        MultivaluedMap<String,String> formData = new MultivaluedMapImpl();
        formData.add("key", key);
        ClientResponse response = makeRequest(getLangsUrl, formData);

        MultivaluedMap<Language, Language> result = new MultivaluedHashMap<>();

        try {
            JSONObject root = new JSONObject(response.getEntity(String.class));
            JSONArray dirs = root.getJSONArray("dirs");
            for (int i = 0; i < dirs.length(); ++i) {
                String direction = dirs.getString(i);
                String[] langs = direction.split("\\-");
                result.add(new YandexAPILanguageImpl(langs[0]),new YandexAPILanguageImpl(langs[1]));
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return result;
    }



    @Override
    public String translate(String text, Language from, Language to) {

        MultivaluedMap<String,String> formData = new MultivaluedMapImpl();
        formData.add("key", key);
        if (from == null) {
            formData.add("lang", to.toString());
        } else {
            formData.add("lang", from.toString() + "-" + to.toString());
        }
        formData.add("text", text);

        ClientResponse response = makeRequest(translateUrl, formData);

        StringBuilder sb = new StringBuilder();
        String responseString = response.getEntity(String.class);
        try {
            JSONObject root =  new JSONObject(responseString);
            JSONArray strs = root.getJSONArray("text");
            for (int i = 0; i < strs.length(); ++i) {
                String str = strs.getString(i);
                sb.append(str);
            }
        } catch (JSONException e) {
            e.printStackTrace();
            System.err.println(responseString);
        }

        return sb.toString();
    }
}
