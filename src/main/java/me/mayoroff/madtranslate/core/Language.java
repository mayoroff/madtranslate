package me.mayoroff.madtranslate.core;

/**
 * Created by Arseni on 22.06.2014.
 */
public abstract class Language {

    public abstract boolean equals(Object other);

    public abstract int hashCode();

    public abstract String toString();
}
