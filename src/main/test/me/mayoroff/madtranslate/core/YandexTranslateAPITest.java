package me.mayoroff.madtranslate.core;

import junit.framework.TestCase;
import me.mayoroff.madtranslate.core.yandex.YandexAPILanguageImpl;
import me.mayoroff.madtranslate.core.yandex.YandexTranslateAPI;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;


public class YandexTranslateAPITest {

    TranslateAPI translateAPI;

    @Before
    public void setUp() {
        translateAPI = new YandexTranslateAPI();
    }

    @Test
    public void testDetectLanguage() throws Exception {
        assertEquals("ru", translateAPI.detectLanguage("добрый вечер").toString());
        assertEquals("en", translateAPI.detectLanguage("good night").toString());
    }

    @Test
    public void testGetTranslations() throws Exception {
        assertTrue(translateAPI.getTranslations().containsKey(new YandexAPILanguageImpl("ru")));
        assertTrue(translateAPI.getTranslations().containsKey(new YandexAPILanguageImpl("en")));
    }

    @Test
    public void testTranslate() throws Exception {
        assertEquals("привет", translateAPI.translate("hello", new YandexAPILanguageImpl("en"), new YandexAPILanguageImpl("ru")));
        assertEquals("hi", translateAPI.translate("привет", null, new YandexAPILanguageImpl("en")));

    }
}